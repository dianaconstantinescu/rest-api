const sqlite3=require('sqlite3').verbose();
const crypto=require('crypto');

let db=new sqlite3.Database('quezzyDatabase.db',sqlite3.OPEN_READWRITE,(err)=>{
    if(err){
        return console.error(err.message);
    }
    console.log('connected to the quezzyDatabase.db');
});

let init=function(){
  db.run("CREATE TABLE IF NOT EXISTS STUDENTI(id_student VARCHAR PRIMARY KEY , email VARCHAR unique," +
                "first_name VARCHAR, last_name VARCHAR,username VARCHAR, password VARCHAR, organization_name VARCHAR," +
                "an_studiu INTEGER, grupa VARCHAR, serie VARCHAR);");
  db.run("CREATE TABLE IF NOT EXISTS PROFESORI(id_profesor VARCHAR PRIMARY KEY, email VARCHAR,first_name VARCHAR , last_name VARCHAR,username VARCHAR, password VARCHAR, organization_name VARCHAR);");
  db.run("CREATE TABLE IF NOT EXISTS TESTE( id_test VARCHAR PRIMARY KEY, nume_test VARCHAR, descriere VARCHAR, domeniu" +
                " VARCHAR, timer integer, public_private integer, feedback integer, punctaj varchar, cale varchar, id_profesor" +
                " integer, foreign key(id_profesor) references  profesori(id_profesor) ); ");
  db.run("CREATE TABLE IF NOT EXISTS SUSTINERI(id_sustinere VARCHAR PRIMARY KEY, id_student integer,id_profesor integer, Foreign key(id_profesor) references profesori(id_profesor)," +
                "Foreign key(id_student) references studenti(id_student));");
  db.run("CREATE TABLE IF NOT EXISTS MATERII( id_materie VARCHAR PRIMARY KEY, denumire varchar );");
  db.run("CREATE TABLE IF NOT EXISTS MATERII_PREDATE( id_mat_predate VARCHAR PRIMARY KEY, id_materie integer, id_profesor integer, " +
                "foreign key(id_profesor) references profesori(id_profesor), foreign key(id_materie) references materii(id_materie));");
  db.run("CREATE TABLE IF NOT EXISTS INTREBARI(id_intrebare VARCHAR PRIMARY KEY, id_test varchar, intrebare varchar, tip varchar," +
               "foreign key(id_test) references Teste(id_test));");  
  db.run("CREATE TABLE IF NOT EXISTS RASPUNSURI(id_raspuns VARCHAR primary key , id_intrebare integer, raspuns varchar," +
                "foreign key(id_intrebare) references Intrebari(id_intrebare));");    
  db.run("CREATE TABLE IF NOT EXISTS REZULTATE(id_rezultat VARCHAR primary key , id_student integer, id_intrebare integer, " +
                "raspuns_ales varchar, raspuns_corect varchar, foreign key(id_student) references studenti(id_student)," +
                "foreign key(id_intrebare) references intrebari(id_intrebare));");
  db.run("CREATE TABLE IF NOT EXISTS CODURI_STUDENTI(id_cod_student VARCHAR primary key , cod integer, email_student varchar, "+
                "data_solicitare_cod date,id_student integer, foreign key(id_student) references studenti(id_student));");
  db.run("CREATE TABLE IF NOT EXISTS CODURI_PROFESORI(id_cod_profesor VARCHAR primary key , cod integer, email_profesor varchar, "+
                "data_solicitare_cod date, id_profesor integer, foreign key(id_profesor) references profesori(id_profesor));");
};

module.exports={
  init:init(),
  db:db
};


db.close((err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('Close the database connection.');
});