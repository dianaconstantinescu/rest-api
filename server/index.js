const express=require('express');
const bodyParser=require('body-parser');
const uuid=require('uuid');
const crypto=require('crypto');
const mysql=require('mysql');


var con = mysql.createConnection({
  host: "quezzy.mysql.database.azure.com",
  user: "andreeacalin@quezzy",
  password: "QuezzyApp!",
  database: "quezzy"
});

//cu post luam date din aplicatia android si le introducem in baza de date
//cu get luam date din db si le trimitem catre aplicatia android
//crypto=to encrypt pass
//uuid=to create unique id string
//body-parser=to parse parameter from api request
//express=to create restful api endpoint
var getRandomString=function(length){
  return crypto.randomBytes(Math.ceil(length/2)).toString('hex');
}

var sha512=function(password, salt){
  var hash=crypto.createHmac('sha512', salt);
  hash.update(password);
  var value=hash.digest('hex');
  return{
    salt:salt,
    passwordHash:value
  }
}

function saltHashPassword(userPassword){
  var salt=getRandomString(16);
  var passwordData=sha512(userPassword, salt);
  return passwordData;
}

function checkHashPassword(userPassword, salt){
  var passwordData=sha512(userPassword, salt);
  return passwordData;
}


const app=express();
app.use(bodyParser.json()); //accept json params
app.use(bodyParser.urlencoded({extended: true})); //accept url encoded params

//const database=require('./dbconfig.js');
//initializare baza de date
//database.init;

app.post('/addStudent/', (req, res, next)=>{
  
  var post_data=req.body;
  
  var uid=uuid.v4();
  var email=post_data.email;
  var first_name=post_data.first_name;
  var last_name=post_data.last_name;
  var username=post_data.username;
  var organization_name=post_data.organization_name;
  var an_studiu=post_data.an_studiu;
  var grupa=post_data.grupa;
  var serie=post_data.serie;
  var plaint_password=post_data.password;
  var hash_data=saltHashPassword(plaint_password);
  var password=hash_data.passwordHash;
  var salt=hash_data.salt;
  
  con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM STUDENTI where username=?', [username],
      function(err, result, fields)
      {
             con.on('error', function(err)
             {
               console.log('MySQL ERROR', err);
              });
      
      
    if(result && result.length)
    {
      res.json('User already exists!');
    }
    else
    {
      con.query('INSERT INTO `Studenti`(`id_student`, `email`, `first_name`, `last_name`, `username`, `password`, `organization_name`, `an_studiu`, `grupa`, `serie`,`salt`) VALUES (?,?,?,?,?,?,?,?,?,?,?)', [uid,email, first_name, last_name, username,password, organization_name, an_studiu, grupa, serie,salt], 
        function(err, result, fields){
          if (err) throw err;
          console.log(result);
      });
      res.json("Student added successfully!");
    }
  });
});
});      
  

  
app.post('/addProfesor/', (req, res, next)=>{
  
  var post_data2=req.body;
  console.log(post_data2);
  var uid2=uuid.v4();
  var email2=post_data2.email;
  var first_name2=post_data2.first_name;
  var last_name2=post_data2.last_name;
  var username2=post_data2.username;
  var organization_name2=post_data2.organization_name;
  var plaint_password2=post_data2.password;
  var hash_data2=saltHashPassword(plaint_password2);
  var password2=hash_data2.passwordHash;
  var salt2=hash_data2.salt;
  
   con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM PROFESORI where username=?', [username2],
      function(err, result, fields)
   {
        con.on('error', function(err)
        {
          console.log('MySQL ERROR', err);
        });
   
        
    if(result && result.length)
    {
      res.json('User already exists!');
    }
    else
    {
      con.query('INSERT INTO `Profesori`(`id_profesor`, `email`, `first_name`, `last_name`, `username`, `password`, `organization_name`,`salt`) VALUES (?,?,?,?,?,?,?,?)', [uid2, email2, first_name2, last_name2, username2,password2, organization_name2,salt2], 
        function(err, result, fields)
        {
          if (err) throw err;
          console.log(result);
        });
      res.json("Teacher added successfully!");
    }
  });

});
});  

app.post('/addTest/', (req, res, next)=>{
  
  var post_data_test=req.body;
  
  var uid_test=uuid.v4();
  var nume_test_test=post_data_test.nume_test;
  var descriere_test=post_data_test.descriere;
  var domeniu_test=post_data_test.domeniu;
  var timer_test=post_data_test.timer;
  var public_private_test=post_data_test.public_private;
  var feedback_test=post_data_test.feedback;
  var punctaj_test=post_data_test.punctaj;
  var cale_test=post_data_test.cale;
  var id_profesor_test=post_data_test.id_profesor;
  
  
  con.connect(function(err) {
    if(err) throw err;
    con.query('INSERT INTO `Teste`(`id_test`, `nume_test`, `descriere`, `domeniu`, `timer`, `public_private`, `feedback`, `punctaj`,`cale`,`id_profesor`) VALUES (?,?,?,?,?,?,?,?,?,?)', [uid_test,nume_test_test, descriere_test, domeniu_test, timer_test,public_private_test, feedback_test, punctaj_test,cale_test,id_profesor_test], 
    function(err, result, fields){
        if (err) throw err;
        console.log(result);
    });
  });
  
  res.json("Test added successfully!");
  
});

app.post('/addCodProfesori/', (req, res, next)=>{
  
  var post_data_codp=req.body;
  
  var uid_codp=uuid.v4();
  var cod_codp=post_data_codp.cod;
  var email_profesor_codp=post_data_codp.email_profesor;
  var data_solicitare_cod_codp=post_data_codp.data_solicitare_cod;
  var id_profesor_codp=post_data_codp.id_profesor;
  
  con.connect(function(err) {
    if(err) throw err;
    con.query('INSERT INTO `Coduri_Profesori`(`id_cod_profesor`, `cod`, `email_profesor`, `data_solicitare_cod`, `id_profesor`) VALUES (?,?,?,?,?)', [uid_codp,cod_codp, email_profesor_codp, data_solicitare_cod_codp,id_profesor_codp], 
    function(err, result, fields){
        if (err) throw err;
        console.log(result);
    });
  });
  
  res.json("Cod profesor adaugat cu success");
  
});

app.post('/addCodStudenti/', (req, res, next)=>{
  
  var post_data_cods=req.body;
  
  var uid_cods=uuid.v4();
  var cod_cods=post_data_cods.cod;
  var email_student_cods=post_data_cods.email_student;
  var data_solicitare_cod_cods=post_data_cods.data_solicitare_cod;
  var id_student_cods=post_data_cods.id_student;
  
  con.connect(function(err) {
    if(err) throw err;
    con.query('INSERT INTO `Coduri_Studenti`(`id_cod_student`, `cod`, `email_student`, `data_solicitare_cod`, `id_student`) VALUES (?,?,?,?,?)', [uid_cods,cod_cods, email_student_cods, data_solicitare_cod_cods,id_student_cods], 
    function(err, result, fields){
        if (err) throw err;
        console.log(result);
    });
  });
  
  res.json("Cod student adaugat cu success");
  
});

app.post('/addIntrebari/', (req, res, next)=>{
  
  var post_data_intr=req.body;
  
  var uid_intr=uuid.v4();
  var id_test_intr=post_data_intr.id_test;
  var intrebare_intr=post_data_intr.intrebare;
  var tip_intr=post_data_intr.tip;
 
  con.connect(function(err) {
    if(err) throw err;
    con.query('INSERT INTO `Intrebari`(`id_intrebare`, `id_test`, `intrebare`, `tip`) VALUES (?,?,?,?)', [uid_intr,id_test_intr, intrebare_intr, tip_intr], 
    function(err, result, fields){
        if (err) throw err;
        console.log(result);
    });
  });
  
  res.json("Intrebare adaugata cu success");
  
});

app.post('/addMaterii/', (req, res, next)=>{
  
  var post_data_mat=req.body;
  
  var uid_mat=uuid.v4();
  var denumire_mat=post_data_mat.denumire;
 
 
  con.connect(function(err) {
    if(err) throw err;
    con.query('INSERT INTO `MATERII`(`id_materie`, `denumire` ) VALUES (?,?)', [uid_mat,denumire_mat], 
    function(err, result, fields){
        if (err) throw err;
        console.log(result);
    });
  });
  
  res.json("Materie adaugata cu success");
  
});

app.post('/addMateriiPredate/', (req, res, next)=>{
  
  var post_data_matp=req.body;
  console.log(post_data_matp);
  var uid_matp=uuid.v4();
  var id_materie_matp=post_data_matp.id_materie;
  var id_profesor_matp=post_data_matp.id_profesor;
 
 
  con.connect(function(err) {
    if(err) throw err;
    con.query('INSERT INTO `MATERII_PREDATE`(`id_mat_predate`, `id_materie`,`id_profesor`) VALUES (?,?,?)', [uid_matp,id_materie_matp,id_profesor_matp], 
    function(err, result, fields){
        if (err) throw err;
        console.log(result);
    });
  });
  
  res.json("Inregistrare adaugata cu success");
  
});

app.post('/addRaspunsuri/', (req, res, next)=>{
  
  var post_data_rasp=req.body;
  
  var uid_rasp=uuid.v4();
  var id_intrebare_rasp=post_data_rasp.id_intrebare;
  var raspuns_rasp=post_data_rasp.raspuns;
 
  con.connect(function(err) {
    if(err) throw err;
    con.query('INSERT INTO `Raspunsuri`(`id_raspuns`, `id_intrebare`,`raspuns`) VALUES (?,?,?)', [uid_rasp,id_intrebare_rasp,raspuns_rasp], 
    function(err, result, fields){
        if (err) throw err;
        console.log(result);
    });
  });
  
  res.json("Raspuns adaugat cu success");
  
});

app.post('/addRezultate/', (req, res, next)=>{
  
  var post_data_rez=req.body;
  
  var uid_rez=uuid.v4();
  var id_student_rez=post_data_rez.id_student;
  var id_intrebare_rez=post_data_rez.id_intrebare;
  var raspuns_ales_rez=post_data_rez.raspuns_ales;
  var raspuns_corect_rez=post_data_rez.raspuns_corect;
 
  con.connect(function(err) {
    if(err) throw err;
    con.query('INSERT INTO `Rezultate`(`id_rezultat`, `id_student`,`id_intrebare`,`raspuns_ales`,`raspuns_corect`) VALUES (?,?,?,?,?)', [uid_rez,id_student_rez,id_intrebare_rez,raspuns_ales_rez,raspuns_corect_rez], 
    function(err, result, fields){
        if (err) throw err;
        console.log(result);
    });
  });
  
  res.json("Rezultat adaugat cu success");
  
});

app.post('/addSustineri/', (req, res, next)=>{
  
  var post_data_sust=req.body;
  
  var uid_sust=uuid.v4();
  var id_student_sust=post_data_sust.id_student;
  var id_profesor_sust=post_data_sust.id_profesor;
 
 
  con.connect(function(err) {
    if(err) throw err;
    con.query('INSERT INTO `SUSTINERI`(`id_mat_predate`, `id_student`,`id_profesor`) VALUES (?,?,?)', [uid_sust,id_student_sust,id_profesor_sust], 
    function(err, result, fields){
        if (err) throw err;
        console.log(result);
    });
  });
  
  res.json("Sustinere adaugata cu success");
  
});

app.post('/loginStudent/', (req, result, next)=>{
  var post_data_loginS=req.body;
  var user_username=post_data_loginS.username;
  var user_password=post_data_loginS.password;
  console.log(post_data_loginS);
  
  con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM STUDENTI where username=?', [user_username],
      function(err, result, fields)
      {
           con.on('error', function(err){
           console.log('MySQL ERROR', err);
          });
   
      
    if(result && result.length)
    {
     var salt=saltHashPassword(user_password);
      var encrypt_password=result[0].password;
      var hashed_password=checkHashPassword(user_password,salt).passwordHash;
      if(encrypt_password == hashed_password){
        result.end(JSON.stringify(result[0]));
      }
      
      else
      {
        result.end(JSON.stringify('Wrong password!'));
      }
    }
    else{
      result.json("Student not exists!");
    }
  });

});
});

app.post('/loginTeacher/', (req, res, next)=>{
  var post_data_loginT=req.body;
  
  var user_password=post_data_loginT.password;
  var user_username=post_data_loginT.username;
  
  con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM PROFESORI where username=?', [user_username],
      function(err, result, fields)
      {
            con.on('error', function(err)
            {
               console.log('MySQL ERROR', err);
            });
     
      
    if(result && result.length)
    {
      var salt=result[0].salt;
      var encrypt_password=result[0].encrypt_password;
      var hashed_password=checkHashPassword(user_password, salt).passwordHash;
      if(encrypt_password == hashed_password)
      {
        res.end(JSON.stringify(result[0]));
      }
      else
      {
        res.end(JSON.stringify('Wrong password!'));
      }
    }
    else
    {
      res.json("Teacher not exists!");
    }
  });

});
});
  
app.get('/test/',(req, res, next)=>{
  console.log('Password: 123456');
  var encrypt=saltHashPassword("123456");
  console.log('Encrypt: '+ encrypt.passwordHash);
  console.log('Salt: '+encrypt.salt);
});

app.get('/getStudenti/',(req, res, next)=>{
   con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM STUDENTI',
      function(err, result, fields)
      {
             con.on('error', function(err)
             {
               console.log('MySQL ERROR', err);
              });
      
      
    if(result && result.length)
    {
      var resultJson = JSON.stringify(result);
            resultJson = JSON.parse(resultJson);
      var apiResult = {};
             apiResult.data = resultJson;
            
            //send JSON to Express
      res.json(apiResult);
    }
    else{
       res.json("There are no students!!!");
    }
    
    });
});
});

app.get('/getProfesori/',(req, res, next)=>{
   con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM PROFESORI',
      function(err, result, fields)
      {
             con.on('error', function(err)
             {
               console.log('MySQL ERROR', err);
              });
      
      
    if(result && result.length)
    {
      var resultJson = JSON.stringify(result);
            resultJson = JSON.parse(resultJson);
      var apiResult = {};
             apiResult.data = resultJson;
            
            //send JSON to Express
      res.json(apiResult);
    }
    else{
       res.json("There are no teachers!!!");
    }
    
    });
});
});

app.get('/getTeste/',(req, res, next)=>{
   con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM TESTE',
      function(err, result, fields)
      {
             con.on('error', function(err)
             {
               console.log('MySQL ERROR', err);
              });
      
      
    if(result && result.length)
    {
      var resultJson = JSON.stringify(result);
            resultJson = JSON.parse(resultJson);
      var apiResult = {};
             apiResult.data = resultJson;
            
            //send JSON to Express
      res.json(apiResult);
    }
    else{
       res.json("There are no tests!!!");
    }
    
    });
});
});
app.get('/getCoduriProfesori/',(req, res, next)=>{
   con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM CODURI_PROFESORI',
      function(err, result, fields)
      {
             con.on('error', function(err)
             {
               console.log('MySQL ERROR', err);
              });
      
      
    if(result && result.length)
    {
      var resultJson = JSON.stringify(result);
            resultJson = JSON.parse(resultJson);
      var apiResult = {};
             apiResult.data = resultJson;
            
            //send JSON to Express
      res.json(apiResult);
    }
    else{
       res.json("There are no codes!!!");
    }
    
    });
});
});

app.get('/getCoduriStudenti/',(req, res, next)=>{
   con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM CODURI_STUDENTI',
      function(err, result, fields)
      {
             con.on('error', function(err)
             {
               console.log('MySQL ERROR', err);
              });
      
      
    if(result && result.length)
    {
      var resultJson = JSON.stringify(result);
            resultJson = JSON.parse(resultJson);
      var apiResult = {};
             apiResult.data = resultJson;
            
            //send JSON to Express
      res.json(apiResult);
    }
    else{
       res.json("There are no tests!!!");
    }
    
    });
});
});


app.get('/getIntrebari/',(req, res, next)=>{
   con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM INTREBARI',
      function(err, result, fields)
      {
             con.on('error', function(err)
             {
               console.log('MySQL ERROR', err);
              });
      
      
    if(result && result.length)
    {
      var resultJson = JSON.stringify(result);
            resultJson = JSON.parse(resultJson);
      var apiResult = {};
             apiResult.data = resultJson;
            
            //send JSON to Express
      res.json(apiResult);
    }
    else{
       res.json("There are no questions!!!");
    }
    
    });
});
});

app.get('/getMaterii/',(req, res, next)=>{
   con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM MATERII',
      function(err, result, fields)
      {
             con.on('error', function(err)
             {
               console.log('MySQL ERROR', err);
              });
      
      
    if(result && result.length)
    {
      var resultJson = JSON.stringify(result);
            resultJson = JSON.parse(resultJson);
      var apiResult = {};
             apiResult.data = resultJson;
            
            //send JSON to Express
      res.json(apiResult);
    }
    else{
       res.json("There are no subjects!!!");
    }
    
    });
});
});

app.get('/getMateriiPredate/',(req, res, next)=>{
   con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM MATERII_PREDATE',
      function(err, result, fields)
      {
             con.on('error', function(err)
             {
               console.log('MySQL ERROR', err);
              });
      
      
    if(result && result.length)
    {
      var resultJson = JSON.stringify(result);
            resultJson = JSON.parse(resultJson);
      var apiResult = {};
             apiResult.data = resultJson;
            
            //send JSON to Express
      res.json(apiResult);
    }
    else{
       res.json("Nu exista materii predate");
    }
    
    });
});
});

app.get('/getRaspunsuri/',(req, res, next)=>{
   con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM RASPUNSURI',
      function(err, result, fields)
      {
             con.on('error', function(err)
             {
               console.log('MySQL ERROR', err);
              });
      
      
    if(result && result.length)
    {
      var resultJson = JSON.stringify(result);
            resultJson = JSON.parse(resultJson);
      var apiResult = {};
             apiResult.data = resultJson;
            
            //send JSON to Express
      res.json(apiResult);
    }
    else{
       res.json("Nu exista raspunsuri!");
    }
    
    });
});
});

app.get('/getRezultate/',(req, res, next)=>{
   con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM REZULTATE',
      function(err, result, fields)
      {
             con.on('error', function(err)
             {
               console.log('MySQL ERROR', err);
              });
      
      
    if(result && result.length)
    {
      var resultJson = JSON.stringify(result);
            resultJson = JSON.parse(resultJson);
      var apiResult = {};
             apiResult.data = resultJson;
            
            //send JSON to Express
      res.json(apiResult);
    }
    else{
       res.json("Nu exista rezultate!");
    }
    
    });
});
});

app.get('/getSustineri/',(req, res, next)=>{
   con.connect(function(err) {
    if(err) throw err;
    con.query('SELECT * FROM RASPUNSURI',
      function(err, result, fields)
      {
             con.on('error', function(err)
             {
               console.log('MySQL ERROR', err);
              });
      
      
    if(result && result.length)
    {
      var resultJson = JSON.stringify(result);
            resultJson = JSON.parse(resultJson);
      var apiResult = {};
             apiResult.data = resultJson;
            
            //send JSON to Express
      res.json(apiResult);
    }
    else{
       res.json("Nu exista sustineri!");
    }
    
    });
});
});


//start server
const port= process.env.PORT || 3000;
app.listen(port, ()=>{
  console.log(`Quezzy Restful running on port ${port}`);
});